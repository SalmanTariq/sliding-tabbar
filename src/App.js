import React,{useState} from 'react';
import SlidingTabBar from './components/SlidingTabBar';

import 'react-material-layout/dist/react-material-layout.min.css';

function App() {
  const [index,setIndex]=useState(0);
  return (
   <SlidingTabBar
   data={['Apple','Banana','Water Melon','Mango','Orange','Stawberry']} 
   activeIndex={index}
   setIndex={setIndex}
    />
  );
}

export default App;
