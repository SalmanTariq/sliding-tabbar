import styled from 'styled-components';

const HEIGHT = 60;
const BAR_SIZE = 24;

export const Wrapper = styled.div`
  width: 100%;
  overflow: hidden;
  max-height: ${HEIGHT}px;
  position: relative;
    border-bottom: 1px solid #000;
  
`;

export const Container = styled.div.attrs({
  'data-layout': 'row',
  'data-layout-align': 'start start',
})`
  min-width: 100%;
  height: ${HEIGHT + BAR_SIZE}px;
  overflow-x: auto;
  overflow-y: hidden;
  /* white-space: nowrap; */
    padding-top: 18px;
    padding-bottom: ${BAR_SIZE}px;
    padding-right: 6px;
    padding-left: 6px;
  
`;

export const Button = styled.div.attrs({
  'data-layout': 'row',
  'data-layout-align': 'start start',
})`
  min-width: 33%;
  /* white-space: nowrap; */
  transition: all 0.3s ease;
  cursor: pointer;
  border-bottom: 4px solid transparent;
  height: ${HEIGHT - 18}px;
  /* display: inline-block; */

  ${({ active }) => `
    padding: 0 18px;
   
    font-weight: ${active ? '700' : '400'};
  `};
`;

export const Arrow = styled.div.attrs({
  'data-layout': 'row',
  'data-layout-align': 'center center',
})`
  position: absolute;
  text-align: center;
  top: 0;
  height: ${HEIGHT}px;
  width: ${HEIGHT / 2}px;
  
    color: #000;
    background: #fff;
  ;

  ${(props) =>
    props.right
      ? `
    right: 0;
  `
      : `left: 0;`}
`;
