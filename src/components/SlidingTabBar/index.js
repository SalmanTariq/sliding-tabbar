/**
 *
 * SlidingTabBar
 *
 */

import debounce from 'lodash/debounce';
import get from 'lodash/get';

import React, { useState, useLayoutEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { useSpring, animated } from 'react-spring';

import ChevronLeft from 'react-feather/dist/icons/chevron-left';
import ChevronRight from 'react-feather/dist/icons/chevron-right';

import useMeasure from '../../hooks/useMeasure';

import * as Styled from './Styled';

const AnimatedContainer = animated(Styled.Container);

const checkElementMin = (el, scroll, width) =>
  el.offsetLeft >= scroll && el.offsetLeft <= scroll + width;

const checkElementMax = (el, scroll, width) =>
  el.offsetLeft + el.offsetWidth >= scroll &&
  el.offsetLeft + el.offsetWidth <= scroll + width;

const getScrollPosition = (activeElement) => {
  try {
    const { offsetLeft, offsetWidth } = activeElement;
    // const elementWidth = activeElement.offsetWidth;
    // return elementLeft - elementWidth * 0.7;
    return offsetLeft - offsetWidth;
  } catch (e) {
    return 0;
  }
};

function SlidingTabBar(props) {
  const [scrollPosition, setScrollPosition] = useState(0);
  const [, setInit] = useState(false);
  const [bind] = useMeasure();
  const offsetWidth = get(bind, 'ref.current.offsetWidth', 0);
  const scrollWidth = get(bind, 'ref.current.scrollWidth', 0);
  const left = getScrollPosition(
    document.getElementById(`item${props.activeIndex}`),
  );
  const scrollAnimation = useSpring({ scroll: left, from: scrollPosition });
  const showArrow = {
    right:
      scrollWidth > offsetWidth && offsetWidth + scrollPosition < scrollWidth,
    left: scrollWidth > offsetWidth && scrollPosition > 24,
  };

  useLayoutEffect(() => {
    const newLeft = getScrollPosition(
      document.getElementById(`item${props.activeIndex}`),
    );
    setScrollPosition(newLeft);
  }, [props.activeIndex]);

  function setPosition(scrollLeft) {
    setScrollPosition(scrollLeft);
    const offsetWidth = get(bind, 'ref.current.offsetWidth', 0);
    const elArr = [];
    props.data.forEach((value, index) => {
      const el = document.getElementById(`item${index}`);
      if (
        checkElementMin(el, scrollLeft, offsetWidth) ||
        checkElementMax(el, scrollLeft, offsetWidth)
      ) {
        elArr.push({ index, el });
      }
    });
    const midPoint = (scrollLeft + (scrollLeft + offsetWidth)) / 2;
    elArr.forEach((value, index) => {
      if (
        value.el.offsetLeft <= midPoint &&
        value.el.offsetLeft + value.el.offsetWidth >= midPoint
      ) {
        props.setIndex(value.index);
      }
    });
  }

  const onScrollHandler = useRef(
    debounce((scrollLeft) => setPosition(scrollLeft), 500),
  ).current;
 
  return (
    <Styled.Wrapper>
      <AnimatedContainer
        {...bind}
        scrollLeft={scrollAnimation.scroll}
        onScroll={(e) => onScrollHandler(e.target.scrollLeft)}
      >
        {props.data.map((item, index) => (
          <Styled.Button
            active={index === props.activeIndex}
            id={`item${index}`}
            key={item}
          >
            {item}
          </Styled.Button>
        ))}
      </AnimatedContainer>
      {!!showArrow.right && (
        <Styled.Arrow right>
          <ChevronRight />
        </Styled.Arrow>
      )}
      {!!showArrow.left && (
        <Styled.Arrow left>
          <ChevronLeft />
        </Styled.Arrow>
      )}
      <span ref={() => setInit(true)} />
    </Styled.Wrapper>
  );
}

SlidingTabBar.propTypes = {
  data: PropTypes.array,
  activeIndex: PropTypes.number,
  setIndex: PropTypes.func.isRequired,
};

SlidingTabBar.defaultProps = {
  data: [],
  activeIndex: 0,
};

export default SlidingTabBar
